<?php

?>
<div style="max-width: 560px;padding: 20px;background: #ffffff;border-radius: 5px;margin:40px auto;font-family: Open Sans,Helvetica,Arial;font-size: 15px;color: #666;">

	<div style="color: #444444;font-weight: normal;">
		<div style="text-align: center;font-weight:600;font-size:26px;padding: 10px 0;border-bottom: solid 3px #eeeeee;">{site_name}</div>
		
		<div style="clear:both"></div>
	</div>
	
	<div style="padding: 0 30px 30px 30px;border-bottom: 3px solid #eeeeee;">

		<div style="padding: 30px 0;font-size: 24px;text-align: center;line-height: 40px;">Tack för att ni har registrerat ert företag hos DataTjej.<span style="display: block;"><p>Ert konto har nu godkänts!</p></span></div>
		<div style="padding: 30px 0;font-size: 18px;text-align: center;line-height: 40px;">Ni kan nu publicera annonser och komma i kontakt med våra medlemmar!</div>
		<div style="padding: 10px 0 50px 0;text-align: center;"><a href="{login_url}" style="background: #555555;color: #fff;padding: 12px 30px;text-decoration: none;border-radius: 3px;letter-spacing: 0.3px;">Logga in på jobbportalen</a></div>
		
		<div style="padding: 0 0 15px 0;">
		
			<div style="background: #eee;color: #444;padding: 12px 15px; border-radius: 3px;font-weight: bold;font-size: 16px;">Kontoinformation</div>
		
			<div style="padding: 10px 15px 0 15px;color: #333;"><span style="color:#999">Din e-postadress:</span> <span style="font-weight:bold">{email}</span></div>
			<div style="padding: 10px 15px 0 15px;color: #333;"><span style="color:#999">Ditt användarnamn:</span> <span style="font-weight:bold">{username}</span></div>
			<div style="padding: 10px 15px 0 15px;color: #333;"><span style="color:#999">Återställ lösenord:</span> <span style="font-weight:bold">{password_reset_link}</span></div>
		
		</div>
		
	</div>
	
	<div style="color: #999;padding: 20px 30px">
		
		<div style="">Vänliga hälsningar!</div>
		<div style=""><a href="{site_url}" style="color: #3ba1da;text-decoration: none;">{site_name}</a></div>
		
	</div>

	<div style="padding: 20px 0 0 0;text-align: center;">
		<a href="https://www.datatjej.se" style="text-decoration: none;">
			<img src="http://www.datatjej.se/logos/datatjej.png" height="32" style="padding: 0 5px 0 5px"/>
		</a>
		<a href="https://www.facebook.com/DataTjej/" style="text-decoration: none;">
			<img src="http://www.datatjej.se/logos/facebook.png" height="32" style="padding: 0 5px 0 5px"/>
		</a>
		<a href="https://www.instagram.com/datatjej/" style="text-decoration: none;">
			<img src="http://www.datatjej.se/logos/instagram.png" height="32" style="padding: 0 5px 0 5px"/>
		</a>
		<a href="https://www.linkedin.com/company/f-reningen-datatjej/" style="text-decoration: none;">
			<img src="http://www.datatjej.se/logos/linkedin.png" height="32" style="padding: 0 5px 0 5px"/>
		</a>
	</div>
	
</div>