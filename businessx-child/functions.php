<?php

// --------------------------- AUTO-GENERATED ---------------------------

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:

if ( !function_exists( 'chld_thm_cfg_parent_css' ) ):
    function chld_thm_cfg_parent_css() {
        wp_enqueue_style( 'chld_thm_cfg_parent', trailingslashit( get_template_directory_uri() ) . 'style.css', array(  ) );
    }
endif;
add_action( 'wp_enqueue_scripts', 'chld_thm_cfg_parent_css', 10 );

// END ENQUEUE PARENT ACTION


// ----------------------------- DEV-TOOLS ------------------------------

// Custom Console logging
// Use at your own discretion
function debugToConsole($msg) {
  echo "<script>console.log(".json_encode($msg).")</script>";
}


// ------------------------- REGISTER ACCOUNT ---------------------------

// Redirect registration 
add_filter('um_login_form_button_two_url', 'my_login_form_button_two_url', 10, 2);
function my_login_form_button_two_url($secondary_btn_url, $args) {
    
    if(strpos($_SERVER['REQUEST_URI'], 'publiceraannons')) {
        $secondary_btn_url = './foretagsregistrering/';
    }
    return $secondary_btn_url;

}

// Only allow selecting birth date more than 18 years ago.
add_filter('um_birth_date_form_edit_field', 'my_form_edit_field', 10, 2);
function my_form_edit_field($output, $mode) {
    $attr = "data-date_max";
    $newValue = date('Y,m,d', strtotime('-18 years'));

    $startPos = strpos ( $output , $attr ) + strlen($attr) + 2;
    $length = strpos($output, " ", $startPos) - $startPos - 1;
    
    $output = substr_replace ( $output , $newValue , $startPos, $length ) ;
    
    return $output;
}


// ------------------------- MY ACCOUNT PAGE ----------------------------

// Filter default fields
add_filter('um_account_tab_general_fields', 'my_account_tab_general_fields', 10, 2);
function my_account_tab_general_fields($args, $shortcode_args) {
    $args = 'user_login';  
    return $args;
}

// Add custom fields
add_action('um_after_account_general', 'show_extra_fields', 100);
function show_extra_fields() {
  
    $id = um_user('ID');
    $isEmployer = get_user_meta($id, "wp_capabilities", true)["employer"] == 1;
    $output = '';
  
    if ($isEmployer)
        $names = array("company_name2", "org_number", "company_contact" , "user_email", "phone_number", "company_url", "invoice_adress", "invoice_reference", "invoice_mail", "company_description");
    else
        $names = array("first_name","last_name", "birth_date", "member_city", "user_email", "phone_number");
    
    $fields = array();
    
    foreach($names as $name)
        $fields[$name] = UM()->builtin()->get_specific_field($name);
    
    $id = um_user('ID');
    $fields = apply_filters('um_account_secure_fields', $fields, $id);

    foreach($fields as $key => $data )
        $output .= UM()->fields()->edit_field($key, $data);
    
    echo $output;
}


// ---------------------- EDIT USER ADMIN PAGE --------------------------

// Remove unused contact methods
add_filter('user_contactmethods', 'update_contact_methods');
function update_contact_methods($contactmethods) {
    unset($contactmethods['aim']);
    unset($contactmethods['jabber']);
    unset($contactmethods['yim']);
    return $contactmethods;
}

// Hide color scheme picker
remove_action('admin_color_scheme_picker', 'admin_color_scheme_picker');
  
// Remove unused fields
add_action('admin_head', 'wordpress_profile_subject_start');
function wordpress_profile_subject_start() {
    $user_role = get_metadata('user', $_GET['user_id'], 'wp_capabilities')[0];
    
    if($user_role) {
        $value = key($user_role);
    }
    
    if($value == "subscriber") {
        ob_start('wordpress_remove_user_personal_options_base');
    } else if($value == "employer") {
        ob_start('wordpress_remove_user_personal_options_employer');
    }
}

function wordpress_remove_user_personal_options_base($personal_options) {
    $personal_options = preg_replace('#<h2>' . __("Personal Options") . '</h2>#s', '', $personal_options, 1);           // Remove the "Personal Options" title
    $personal_options = preg_replace('#<h2>' . __("Name") . '</h2>#s', '', $personal_options, 1);                       // Remove the "Name" title
    $personal_options = preg_replace('#<h2>' . __("Contact Info") . '</h2>#s', '', $personal_options, 1);               // Remove the "Contact Info" title
    $personal_options = preg_replace('#<h2>' . __("About Yourself") . '</h2>#s', '', $personal_options, 1);             // Remove the "About Yourself" title
    $personal_options = preg_replace('#<tr class="user-rich-editing-wrap(.*?)</tr>#s', '', $personal_options, 1);       // Remove the "Visual Editor" field
    $personal_options = preg_replace('#<tr class="user-comment-shortcuts-wrap(.*?)</tr>#s', '', $personal_options, 1);  // Remove the "Keyboard Shortcuts"
    $personal_options = preg_replace('#<tr class="user-display-name-wrap(.*?)</tr>#s', '', $personal_options, 1);       // Remove the "Display name publicly as" field
    $personal_options = preg_replace('#<tr class="user-url-wrap(.*?)</tr>#s', '', $personal_options, 1);                // Remove the "Website" field
    $personal_options = preg_replace('#<tr class="user-profile-picture(.*?)</tr>#s', '', $personal_options, 1);         // Remove the "Profile Picture" field
    $personal_options = preg_replace('#<tr class="user-description-wrap(.*?)</tr>#s', '', $personal_options, 1);        // Remove the "Biographical Info" field
    $personal_options = preg_replace('#<tr class="show-admin-bar(.*?)</tr>#s', '', $personal_options, 1);               // Remove the "Toolbar" field
    $personal_options = preg_replace('#<tr class="user-language-wrap(.*?)</tr>#s', '', $personal_options, 1);           // Remove the "Language" field
    $personal_options = preg_replace('#<h2>' . __("About the user") . '</h2>#s', '', $personal_options, 1);
    $personal_options = preg_replace('#<(.*)categoryPosts(.*)>#m', '', $personal_options, -1);
    $personal_options = preg_replace('#<h3>' . __("Ultimate Member") . '</h3>#s', '', $personal_options, 1);
    $personal_options = preg_replace('#<div id="um_role_selector_wrapper(.*?)</div>#s', '', $personal_options, 1);      // Remove the "Language" field
    
    return $personal_options;
}

function wordpress_remove_user_personal_options_employer($personal_options) {
    $personal_options2 = wordpress_remove_user_personal_options_base($personal_options);
    $personal_options2 = preg_replace('#<tr class="user-first-name-wrap(.*?)</tr>#s', '', $personal_options2, 1);
    $personal_options2 = preg_replace('#<tr class="user-last-name-wrap(.*?)</tr>#s', '', $personal_options2, 1);
    
    return $personal_options2;
}

add_action('admin_footer', 'wordpress_profile_subject_end');
function wordpress_profile_subject_end() {
    ob_end_flush();
}


// ---------------------- JOB MANAGER CUSTOM FORMS ----------------------

// Change and add new fields to the job submission form
add_filter('submit_job_form_fields', 'change_form_fields');
function change_form_fields ( $fields ) {

    if(!isset($fields['company'])) 
        return $fields;

    // Remove unused default fields
    if(isset($fields['company']['phone'])) 
        unset($fields['company']['phone']);
    if(isset($fields['company']['company_video'])) 
        unset($fields['company']['company_video']);
    if(isset($fields['company']['company_tagline'])) 
        unset( $fields['company']['company_tagline']);
    if(isset($fields['company']['company_twitter'])) 
        unset( $fields['company']['company_twitter']);
    if(isset($fields['company']['company_logo'])) 
        unset( $fields['company']['company_logo']);

    // Change existing fields
    $fields['job']['job_title']['placeholder'] = __('Junior frontend-utvecklare', 'job_manager');
    $fields['job']['job_title']['oninvalid'] = 'Var vänlig fyll i vilken position annonsen berör.';
    $fields['job']['job_description']['description'] = __('Om ansökningen ska ske via e-post, glöm inte  att ange vilken information som den ansökande ska skicka med');
    $fields['job']['job_deadline']['description'] = __('');
    $fields['job']['application']['description'] = __('Ansökan via URL kräver att du anger "http://"');
    $fields['job']['application']['oninvalid'] = 'Var vänlig fyll i en emailadress eller webbadress för ansökan.';
    $fields['company']['company_website']['required'] = true;
    $fields['company']['company_website']['placeholder'] = __('http://www.exempel.se');
    $fields['company']['company_website']['oninvalid'] = 'Var vänlig fyll i företagets hemsida.';
    $fields['company']['company_name']['oninvalid'] = 'Var vänlig fyll i namnet på företaget.';
    
    // Add new fields to job submission form
    $fields['company']['company_description'] = array(
        'label'       => __( 'Företagsbeskrivning', 'job_manager' ),
        'type'        => 'textarea',
        'required'    => true,
        'oninvalid'   => 'Var vänlig fyll i en företagsbeskrivning',
        'placeholder' => __('Ange en beskrivning av ditt företag', 'job_manager'),
        'priority'    => 2
    );
    $fields['job']['job_publish_date'] = array(
        'label'       => __( 'Publiceringsdatum', 'job_manager' ),
        'description' => __('Datum när annonsen ska läggas upp', 'job_manager'),
        'type'        => 'date',
        'required'    => true,
        'oninvalid'   => 'Var vänlig fyll i vilket datum annonsen ska publiceras',
        'priority'    => 8
    );
    $fields['job']['payment_plan'] = array(
        'label'       => __( 'Betalningsplan', 'job_manager' ),
        'type'        => 'select',
        'required'    => true,
        'priority'    => 14,
        'options'     => array(30 => '30 dagar - 3 000 KR', 60 => '60 dagar - 6 000 KR', 90 => '90 dagar - 9 000 KR', 180 => '6 månader - 15 000 KR')
    );
    $fields['job']['payment_discount_code'] = array(
        'label'       => __( 'Rabattkod', 'job_manager' ),
        'type'        => 'text',
        'required'    => false,
        'placeholder' => 'Ange eventuell rabattkod',
        'priority'    => 15
    );
    $fields['job']['accept_terms'] = array(
        'label'       => __( 'Köpvillkor', 'job_manager' ),
        'type'        => 'checkbox',
        'description' => 'Jag har läst och godkänner DataTjejs <a href="/villkor">köpvillkor</a>',
        'required'    => true,
        'oninvalid'   => 'För att kunna skapa en annons måste du först godkänna DataTjejs köpvillkor.',
        'priority'    => 20
    );

    return $fields;
}

// Prefill company information to publishing form where applicable
add_filter('submit_job_form_fields_get_user_data', 'custom_submit_job_form_fields_get_user_data', 10, 2);
function custom_submit_job_form_fields_get_user_data( $fields, $user_id ) {

    $name = get_metadata( 'user', $user_id, 'company_name2');
    $website = get_metadata( 'user', $user_id, 'company_url');
    $logo = get_metadata( 'user', $user_id, 'account_image');
    $description = get_metadata( 'user', $user_id, 'company_description');

    $fields['company']['company_name']['value'] = $name[0];
    $fields['company']['company_description']['value'] = $description[0];
    $fields['company']['company_website']['value'] = $website[0];
    $fields['job']['application']['value'] = '';

    return $fields;
}


// -------------------------- JOB LISTING PAGE --------------------------

// Add categories
add_filter ('job_manager_job_filters_after', 'categories_heading');
function categories_heading() {
    echo '<ul class="ul_test">
            <li class="li_test">Position</li>
            <li class="li_test">Företag</li>
            <li class="li_test">Plats</li>
            <li class="li_test">Publicerad</li>
          </ul>';
}


// -------------------------- JOB DETAILS PAGE --------------------------

// Job title 
add_action('single_job_listing_start', 'add_job_title');
function add_job_title() {
    global $post;
    $title = get_post_meta($post->ID, '_job_title', true);
    $company = get_post_meta($post->ID, '_company_name', true);
    $website = get_post_meta($post->ID, '_company_website', true);
    
    echo '<header class="job_header"><h1 class="blue-underline">' . $title . '</h1>';
    echo '<a href="' . $website . '"><h3>' . $company . '</h3></a>';
}
add_action('single_job_listing_meta_end', function() { echo '</header>'; } );

// Job description 
add_action('single_job_listing_meta_after', 'add_job_description' );
function add_job_description() {
    global $post;

    echo '<div class="job_description"><h3>Beskrivning av tjänst</h3><pre class="description_text">';
    wpjm_the_job_description();
    echo ('</pre></div>');
}

// Company details
add_action('single_job_listing_end', 'add_company_description');
function add_company_description() {
    global $post;

    echo '<div class="company_description"><h3>Beskrivning av företaget</h3>';
    $description = get_post_meta($post->ID, '_company_description', true);
    echo '<pre class="description_text">' . $description . '</pre></div>';
}

// Remove job tags
add_filter('enable_job_tag_archives', 'remove_tags');
function remove_tags() {
    remove_all_filters('the_job_description');
}

// Change date-time format to application deadline
add_filter('job_manager_application_deadline_closing_date_display', 'change_date_format');
function change_date_format() {
    global $post;
    $date = get_post_meta($post->ID, '_application_deadline', true);
    return date_i18n('d M', strtotime($date));
}

// Hide page if user not logged in
add_action ('single_job_listing_meta_before', 'custom_single_job_page_secret');
function custom_single_job_page_secret() {	
	if ( !is_user_logged_in() ) {
		echo '<div class="hideJobPage">';
	} 
}
add_action ('single_job_listing_end', 'custom_single_job_page');
function custom_single_job_page() {
	if ( !is_user_logged_in() ) {
		echo '</div>';
		echo 'Klicka <a href="/login/" class="style_link">här</a> för att logga in och se hela annonsen.';
	} 
}


// ------------------------- MY JOBS LISTINGS ---------------------------

// Allow user to edit expired jobs
add_filter ('job_manager_user_can_edit_pending_submissions', 'job_manager_able_to_custom_expired');
function job_manager_able_to_custom_expired() {	
	return true;
}

// Adjust columns
add_filter('job_manager_job_dashboard_columns', 'custom_job_manager_job_dashboard_columns');
function custom_job_manager_job_dashboard_columns( $columns ) {
    unset( $columns['filled'] );
    unset( $columns['date'] ); 
    unset( $columns['expires_or_closing_date'] );
    unset( $columns['closing_date'] );
    $columns['location'] = 'Plats';
    $columns['type'] = 'Typ av tjänst';
    $columns['publish_date'] = 'Publicerad';
    $columns['expiration_date'] = 'Slutdatum';
    
    return $columns;
}

// Fill custom columns with data
add_action('job_manager_job_dashboard_column_location', function($post) {
    $location = get_post_meta($post->ID, '_job_location', true);
    echo $location;
});

add_action('job_manager_job_dashboard_column_type', function($post) {
    $type = the_terms($post->ID, 'job_listing_type', '', ',');
    echo $type;
});

add_action('job_manager_job_dashboard_column_publish_date', function($post) {
    $date = get_post_meta($post->ID, '_job_publish_date', true);
    echo date_i18n('d M', strtotime($date));
});

add_action('job_manager_job_dashboard_column_expiration_date', function($post) {
    $date = get_post_meta($post->ID, '_job_expiration_date', true);
    echo date_i18n('d M', strtotime($date));
});


// -------------------- EDIT JOB LISTING ADMIN PAGE ---------------------

// Change fields on edit/details page
add_filter( 'job_manager_job_listing_data_fields', 'custom_job_manager_job_listing_data_fields' );
function custom_job_manager_job_listing_data_fields($fields) {

    // Remove unnecessary default fields
    if(isset($fields['_company_phone'])) 
        unset($fields['company']['phone']);
    if(isset($fields['_company_tagline'])) 
        unset($fields['_company_tagline']);
    if(isset($fields['_company_twitter'])) 
        unset($fields['_company_twitter']);
    if(isset($fields['_company_video'])) 
        unset($fields['_company_video']);
    if(isset($fields['_job_expires'])) 
        unset($fields['_job_expires']);

    // Add new fields 
    $fields['application_deadline'] = array(
        'type'        => 'date'
    );
    $fields['_application_deadline']['label'] = 'Senaste ansökningsdatum';

    $fields['job_expiration_date'] = array(
        'type'        => 'date'
    );
    $fields['_job_expiration_date']['label'] = 'Utgångsdatum';

    $fields['_job_discount_code'] = array(
        'label'       => 'Rabattkod',
        'type'        => 'text'
    );

    $fields['_company_description'] = array(
        'label'       => 'Företagsbeskrivning',
        'type'        => 'textarea'
    );

    $fields['_company_logo'] = array(
        'label'       => 'Logotyp',
        'type'        => 'file'
    );

    $fields['_company_website']['label'] = 'Företagets hemsida';

    // Set field order
    $fields['_job_location']['priority'] = 1;
    $fields['_application']['priority'] = 2;
    $fields['_job_expiration_date']['priority'] = 3;
    $fields['_application_deadline']['priority'] = 4;
    $fields['_job_discount_code']['priority'] = 5;
    $fields['_company_name']['priority'] = 7;
    $fields['_company_description']['priority'] = 8;
    $fields['_company_website']['priority'] = 9;
    $fields['_company_logo']['priority'] = 10;
    $fields['_job_author']['priority'] = 11;
    $fields['_filled']['priority'] = 12;
    $fields['_featured']['priority'] = 13;

    return $fields;
}


// ---------------------- JOB LISTINGS ADMIN PAGE -----------------------

// Add custom category columns
add_filter('manage_job_listing_posts_columns', 'custom_job_listing_columns');
function custom_job_listing_columns($defaults) {
    
    $defaults['contact'] = __('Kontakt', 'jobs');
    $defaults['org_number'] = __('Organisationsnummer', 'jobs');

	return $defaults;
}

// Fill columns with data
add_action('manage_job_listing_posts_custom_column', 'custom_job_listing_column_value', 10, 2);
function custom_job_listing_column_value($column_name, $post_ID) {
    if ($column_name == 'contact') {

        $post = get_post($post_ID);
        $author = $post->post_author;
        $result = get_userdata($author);;

        echo $result->user_email;
	}
    if ($column_name == 'org_number') {

        $post = get_post($post_ID);
        $author = $post->post_author;
        $result = get_user_meta($author, 'org_number', true);

        echo $result;
    }
}

// Styling
add_action('admin_head', 'my_custom_styling');
function my_custom_styling() {
    echo '<style>
        tbody#the-list tr.clearfix::before {
            content: none;
        }
        .job_position company_logo {
            position: relative;
            left: 0;
            right: 0;
            bottom: 0;
            top: 0;
        }
        .username.column-username img{
            max-height: 100px;
        }

        .button.button-icon.tips.icon-approve {
            display: none;
        }
    </style>';
}


// ------------------------------ EMAILS --------------------------------

// Redirect user after email confirmation
add_action('um_after_email_confirmation', 'my_after_email_confirmation', 10, 1);
function my_after_email_confirmation($user_id) {
    $redirect = (um_user('url_email_activate')) ? um_user('url_email_activate') : um_get_core_page('account', 'account_active');
    exit(wp_redirect($redirect));
}

// Compose user deletion mail
add_action('um_delete_user', 'my_delete_user', 10, 1);
function my_delete_user($user_id) {
  
    $user_info = get_userdata($user_id);
    $email = $user_info->user_email;
    
    $headers = "From: DataTjej <it@datatjej.se>\r\n
                MIME-Version: 1.0\r\n
                Content-Type: text/html; charset=UTF-8\r\n";
              
    $msg = "<html><body>
                <div style='max-width: 560px;padding: 20px;background: #ffffff;border-radius: 5px;margin:40px auto;font-family: Open Sans,Helvetica,Arial;font-size: 15px;color: #666;'>
                    <div style='color: #444444;font-weight: normal;'>
                        <div style='text-align: center;font-weight:600;font-size:26px;padding: 10px 0;border-bottom: solid 3px #eeeeee;'>
                            DataTjej
                        </div>
                        <div style='clear:both'></div>
                    </div>
                    <div style='padding: 0 30px 0 30px;border-bottom: 3px solid #eeeeee;'>
                        <div style='padding: 30px 0;font-size: 22px;text-align: center;line-height: 40px;'>
            ";

    if(get_user_meta($user_id, "wp_capabilities", true)["employer"] == 1){
    
        $msg .= "Ert konto har raderats.
                <div style='font-size: 18px;'>
                    All information och uppladdningar har blivit permanent borttaget. 
                </div>
            </div>
            <div style='padding: 15px; background: #eee; border-radius: 3px; text-align: center;'>
                Om ert konto har raderats felaktigt. 
                <a style='color: #3ba1da; text-decoration: none;' href='mailto:{admin_email}'>Kontakta oss</a> så hjälper vi er!
            ";

    } else if(get_user_meta($user_id, "wp_capabilities", true)["subscriber"] == 1){
    
        $msg .= "Ditt konto hos DataTjej har raderats. Du är inte längre medlem hos DataTjej. 
                <div style='font-size: 18px;'>
                    All personlig information och uppladdningar har blivit permanent borttaget ur databasen.  
                </div>
            </div>
            <div style='padding: 15px; background: #eee; border-radius: 3px; text-align: center;'>
                Om ditt konto har raderats felaktigt. 
                <a style='color: #3ba1da; text-decoration: none;' href='mailto:{admin_email}'>Kontakta oss</a> så hjälper vi dig!
            </div>
            <div style='font-size: 18px; padding: 15px 0;'>
                Du är varmt välkommen att i framtiden bli medlem igen hos DataTjej!
            ";
    }
    
    $msg .= "</div>
        </div>
            <div style='padding: 10px 0 0 0;text-align: center;'>
                Vänliga hälsningar
            </div>
            <div style='padding: 10px 0 0 0;text-align: center;'>
                <a href='https://www.datatjej.se' style='color: #666;text-decoration: none;'>
                    DataTjej
                </a>
            </div>
            <div style='padding: 20px 0 0 0;text-align: center;'>
                <a href='https://www.datatjej.se' style='text-decoration: none;'>
                <img src='http://www.datatjej.se/logos/datatjej.png' height='32' style='padding: 0 5px 0 5px'/>
                </a>
                <a href='https://www.facebook.com/DataTjej/' style='text-decoration: none;'>
                <img src='http://www.datatjej.se/logos/facebook.png' height='32' style='padding: 0 5px 0 5px'/>
                </a>
                <a href='https://www.instagram.com/datatjej/' style='text-decoration: none;'>
                <img src='http://www.datatjej.se/logos/instagram.png' height='32' style='padding: 0 5px 0 5px'/>
                </a>
                <a href='https://www.linkedin.com/company/f-reningen-datatjej/' style='text-decoration: none;'>
                <img src='http://www.datatjej.se/logos/linkedin.png' height='32' style='padding: 0 5px 0 5px'/>
                </a>
            </div>
        </div>
    </body></html>";
    
  //Send mail
  wp_mail($email, "Raderat konto" ,$msg, $headers); 
}


// ---------------------------- SHORTCODES ------------------------------

// Get amount of active job listings
// $atts = [(optional) string title, (optional) string order='asc'|'desc']
// if order != returns int
// else returns html
add_shortcode('count_job_listings', 'get_active_job_listings');
function get_active_job_listings( $atts ) {

  // Check if parameters are provided, othwerwise assign them set values
	$a = shortcode_atts( array(
		'title' => false,
		'order' => false,
    'limit' => 3
	), $atts );

    // Check if title is provided, if so add title-sql to query, otherwise add nothing
    $title = $a['title'];
    if(!$title) {
        $end_query = "";
    } else {
        $end_query = "AND ps.post_title = '".$title."'";
    }

    global $wpdb;

    // If order is provided, make custom sql-request and return html with results, otherwise return count of results
    $order = $a['order'];
    if(!$order) {

        $result = $wpdb->get_row("SELECT COUNT(*) AS count
                                  FROM wp_posts AS ps
                                  JOIN wp_postmeta AS pm ON ps.ID=pm.post_id
                                  WHERE pm.meta_key = '_filled' AND pm.meta_value = 0
                                  AND ps.post_type = 'job_listing' AND ps.post_status = 'publish'
                                  ".$end_query
                                );
        return $result->count;

    } else {

        $order = ($order == "asc" ? "ASC" : "DESC");

        $limit = $a['limit'];

        $results = $wpdb->get_results("SELECT post_title, COUNT(*) as count
                                       FROM wp_posts
                                       WHERE post_type = 'job_listing' AND post_status = 'publish'
                                       GROUP BY post_title
                                       ORDER BY count ".$order."
                                       LIMIT ".$limit);

        $return_string = "";
        $i = 0;
        
        foreach ($results as $key => $result) {

            $title = $result->post_title;
            $count = $result->count;
            $return_string .= "<span class='job_bold'>";
            $return_string .= $title;
            $return_string .= "</span>";

            if($i == count($results) - 2) {
                $return_string .= " & ";
            } else if($i == count($results) - 1){
                $return_string .= ".";
            } else {
                $return_string .= ", ";
            }

            $i++;
        }

        return $return_string;
    }
}

// Get name of logged in user
add_shortcode('loggedin_name', 'get_loggedin_name');
function get_loggedin_name( ) {
    return um_user('user_login');
}

// Add logout icon
add_shortcode('logout_icon', 'create_logout_icon');
function create_logout_icon($atts) {
    return "<div class='logout_icon ion-log-out' title='Logga ut'></div>";
}

// Add gender notification
add_shortcode('gender_info', 'get_gender_info');
function get_gender_info($atts) {
    $msg = '<p>Endast kvinnor och icke-binära kan bli medlemmar i DataTjej.</p>';
    return $msg;
}

// Add age notification
add_shortcode('age_info', 'get_age_info');
function get_age_info($atts) {
  $msg = '<p>Du måste vara minst 18 år för att kunna bli medlem i DataTjej.</p>';
  return $msg;
}