<?php
/**
 * Job listing in the loop.
 *
 * This template can be overridden by copying it to yourtheme/job_manager/content-job_listing.php.
 *
 * @see         https://wpjobmanager.com/document/template-overrides/
 * @author      Automattic
 * @package     WP Job Manager
 * @category    Template
 * @since       1.0.0
 * @version     1.27.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

global $post;
?>
<li <?php job_listing_class(); ?> data-longitude="<?php echo esc_attr( $post->geolocation_lat ); ?>" data-latitude="<?php echo esc_attr( $post->geolocation_long ); ?>">
	<a href="<?php the_job_permalink(); ?>">
	    <!-- Ingen bild ska synas här
		<?php the_company_logo(); ?> -->
		<div class="position">
			<h3><?php wpjm_the_job_title(); ?></h3>
		</div>
		<div class="company">
				<?php the_company_name( '<strong>', '</strong> ' ); ?>
				<!-- Tagline används inte
				<?php the_company_tagline( '<span class="tagline">', '</span>' ); ?> -->
			</div>
		<div class="location">
			<?php the_job_location( false ); ?>
		</div>
        <div class="published">
			<?php 
				global $post;
				$date = date_i18n('d M', strtotime($post->post_date));
				echo($date);
				//the_job_publish_date(); 
			?>
        </div>
		<!-- 
		<ul class="meta">
			<?php do_action( 'job_listing_meta_start' );?>
            Shows application deadline. Comes from application deadline addon that uses hooks to add data.
			<?php do_action( 'job_listing_meta_end' ); ?>
		</ul>
        <ul class="meta_after">
            <?php if ( get_option( 'job_manager_enable_types' ) ) { ?>
				<?php $types = wpjm_get_the_job_types(); ?>
				<?php if ( ! empty( $types ) ) : foreach ( $types as $type ) : ?>
					<li class="job-type <?php echo esc_attr( sanitize_title( $type->slug ) ); ?>"><?php echo esc_html( $type->name ); ?></li>
				<?php endforeach; endif; ?>
			<?php } ?>       
        </ul> -->
	</a>
</li>