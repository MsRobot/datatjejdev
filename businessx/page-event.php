<?php
/* ------------------------------------------------------------------------- *
 * Page Event
/* ------------------------------------------------------------------------- */

/**
 * Filtered CSS classes
 * ------
 * section: grid-wrap
 * div: grid-container grid-1 padding-small clearfix
 * main: grid-col grid-posts-col site-page clearfix
 * ------
 */

// Header and Footer templates
$businessx_header_tmpl = apply_filters( 'businessx_header___tmpl', '' );
$businessx_footer_tmpl = apply_filters( 'businessx_footer___tmpl', '' );

// Header
get_header( $businessx_header_tmpl );

// Title
businessx_get_heading_templ( 'page', 'full-width' );
?>

<section role="main" id="content" class="<?php businessx_occ( 'businessx_page___section_classes' ); ?>">
	<?php do_action( 'businessx_page__inner_sec_top' ); ?>

	<div class="<?php businessx_occ( 'businessx_page___container_classes' ); ?>">

		<?php do_action( 'businessx_page__inner_before' ); ?>

		<main id="main" class="<?php businessx_occ( 'businessx_page___main_classes' ); ?>" role="main">
            <?php
			while ( have_posts() ) : the_post();
				// Page template
				get_template_part( 'partials/posts/content', 'page' );

			endwhile;
			?>
        </main>
        <?php do_action( 'businessx_page__inner_after' ); ?>
    </div>
    <?php do_action( 'businessx_page__inner_sec_bottom' ); ?>
</section>

<!-- EVENT POST SECTION -->
<?php if ( (get_category_by_slug('kommande-event')->category_count > 0) ||  
        (get_category_by_slug('tidigare-event')->category_count > 0) ) { ?>
    <section class="article-list grid-wrap center">

        <?php if (get_category_by_slug('kommande-event')->category_count > 0) { ?>

            <h2 class="more-mg">
                <u class="white-underline">Kommande event</u>
            </h2>
            <?php query_posts('category_name=kommande-event&showposts=10'); while (have_posts()) : the_post(); ?>
                <article class="event">
                    <a href="<?php the_permalink(); ?>" class="article">
                        <?php if ( has_post_thumbnail() ) { ?>
                            <div class="post-thumb">
                                <?php the_post_thumbnail( 'businessx-tmb-blog-normal' ); ?>
                            </div>
                            <div class="post-info">
                                <h3 class="post-lg"><?php the_title(); ?></h2>
                                <?php the_excerpt(); ?>
                            </div>
                            <div class="post-button">
                                <button type="button">Läs mer</button>
                            </div>
                            
                        <?php } else { ?>
                            <div class="post-info">
                                <h3 class="post-lg"><?php the_title(); ?></h2>
                                <?php the_excerpt(); ?>
                            </div>
                            <div class="post-button">
                                <button type="button">Läs mer</button>
                            </div>
                        <?php } ?>
                    </a>
                </article>
            <?php endwhile; ?>
            <?php wp_reset_query(); 
        } ?>
        <?php if (get_category_by_slug('tidigare-event')->category_count > 0) { ?>

            <h2 class="more-mg">
                <u class="white-underline">Tidigare event</u>
            </h2>
            <?php query_posts('category_name=tidigare-event&showposts=10'); while (have_posts()) : the_post(); ?>
                <article class="event old-post">
                    <a href="<?php the_permalink(); ?>" class="article">
                        <?php if ( has_post_thumbnail() ) { ?>
                            <div class="post-thumb">
                                <?php the_post_thumbnail( 'businessx-tmb-blog-normal' ); ?>
                            </div>
                            <div class="post-info">
                                <h3 class="post-lg"><?php the_title(); ?></h3>
                                <?php the_excerpt(); ?>
                            </div>
                            <div class="post-button">
                                <button type="button">Läs mer</button>
                            </div>
                        <?php } else { ?>
                            <div class="post-info">
                                <h3 class="post-lg"><?php the_title(); ?></h3>
                                <?php the_excerpt(); ?>
                            </div>
                            <div class="post-button">
                                <button type="button">Läs mer</button>
                            </div>
                        <?php } ?>
                    </a>
                </article>
            <?php endwhile; ?>
            <?php wp_reset_query(); 
        }?>	
    </section>
<?php } ?>

<?php
// Footer
get_footer( $businessx_footer_tmpl ); 
?>
